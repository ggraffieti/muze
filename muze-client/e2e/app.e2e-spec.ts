import { MuzeClientPage } from './app.po';

describe('muze-client App', () => {
  let page: MuzeClientPage;

  beforeEach(() => {
    page = new MuzeClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
