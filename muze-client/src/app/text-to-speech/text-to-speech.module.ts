import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CookieManagerModule } from '../cookie-manager/cookie-manager.module';
import * as mespeak from 'mespeak';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})

export class TextToSpeechModule {

  private enPath: string = "assets/mespeak/voices/en/en-us.json";
  private frPath: string = "assets/mespeak/voices/fr.json";
  private itPath: string = "assets/mespeak/voices/it.json";

  private s;

  constructor(private cookieManger: CookieManagerModule) {
    if (!mespeak.isConfigLoaded()) {
      mespeak.loadConfig("assets/mespeak/mespeak_config.json");
    }
    this.changeVoice(this.cookieManger.getLanguage());
  }

  public speak(text: string, fun: () => void) {
    this.s = mespeak.speak(text, {}, fun);
  }

  public stop() {
    mespeak.stop(this.s);
  }

  public changeVoice(newVoice: string) {
    switch (newVoice) {
      case "en": mespeak.loadVoice(this.enPath); break;
      case "fr": mespeak.loadVoice(this.frPath); break;
      case "it": mespeak.loadVoice(this.itPath); break;
      default: mespeak.loadVoice(this.enPath);
    }
  }
}
