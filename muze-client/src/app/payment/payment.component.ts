import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { CheckPayment } from '../checkPayment';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

  public creditCards: string[] = [
    "Visa", "Mastercard", "American Express"
  ];

  model = new Payment('', '', this.creditCards[0], '');

  submitted = false;



  constructor(private checkPayment: CheckPayment, private router: Router) { }

  ngOnInit() {
  }

  public pay() {
    this.submitted = true;
    this.checkPayment.pay();
    this.router.navigateByUrl('/qrticket');
  }

}

class Payment {
  constructor(public name: string, public surname: string, public cc: string, public ccNumber: string) {}
}
