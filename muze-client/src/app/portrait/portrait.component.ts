import { Component, OnInit, ApplicationRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, URLSearchParams } from '@angular/http';
import { ServerConfig } from "../server-config";
import { TextToSpeechModule } from '../text-to-speech/text-to-speech.module';
import { TranslationManagerModule } from '../translation-manager/translation-manager.module';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-portrait',
  templateUrl: './portrait.component.html',
  styleUrls: ['./portrait.component.scss']
})
export class PortraitComponent implements OnInit {

  private id: string;
  private portraitName: string;
  private portraitDesc: string;
  private portraitYear: number;
  private portraitArtist: string;
  private isMobile: boolean = true;
  private foundPortrait = true;
  private showLoading;
  private isSpeaking:boolean = false;
  private speakIcon: string = "speak.png";
  private serverConfig: ServerConfig = new ServerConfig();
  private imgURL: string;
  private showLang = false;
  private s;

  private object;

  constructor(private route: ActivatedRoute,
    private http: Http,
    private appRef: ApplicationRef,
    private _translate: TranslationManagerModule,
    private textToSpeech: TextToSpeechModule) {
    this.route.params.subscribe(params => this.id = params['id']);
    this.imgURL = this.serverConfig.getServerAddress() + "/img/" +this.id;
  }

  ngOnInit() {
    this.subscribeToLangChanged();
    if (this.isMobile) {
      this.getPortraitInfo();
    }
  }

  subscribeToLangChanged() {
    if (this.isMobile) {
      return this._translate.onLangChanged.subscribe(x => this.getPortraitInfo());
    }
  }

  private getPortraitInfo() {
    const lang = this._translate.language;
    const url = this.serverConfig.getServerAddress() + '/info/' + this.id;
    const params = new URLSearchParams();
    params.set('lang', lang);
    this.http.get(url, { search: params })
      .map((res) => this.extractData(res))
      .subscribe();
  }

  private extractData(res: Response) {
    let body = res.json();
    if (Object.keys(body).length === 0) {
      this.foundPortrait = false;
    } else {
      this.portraitName = body.name;
      this.portraitDesc = body.description;
      this.portraitArtist = body.artist;
      this.portraitYear = body.year;
    }
  }

  private actuallySpeak() {
    setTimeout(() => {
      this.textToSpeech.speak(this.portraitDesc, this.speechEndCallback.bind(this));
      this.showLoading = false;
      this.isSpeaking = true;
      this.speakIcon = "stopSpeak.png";
    }, 100)
  }

  public speak() {
    if (!this.isSpeaking) {
      this.showLoading = true;
      this.actuallySpeak();
    } else {
      this.textToSpeech.stop();
      this.isSpeaking = false;
      this.speakIcon = "speak.png";
    }
  }

  public switchLangVis() {
    this.showLang = this.showLang == true ? false : true;
  }

  private speechEndCallback() {
    this.isSpeaking = false;
    this.speakIcon = "speak.png";
    this.appRef.tick();
  }
}
