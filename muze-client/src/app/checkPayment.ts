import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

@Injectable()
export class CheckPayment implements CanActivate {

  private paid: boolean = false;

  constructor() {}

  public pay() {
    this.paid = true;
  }

  canActivate() {
    return this.paid;
  }
}
