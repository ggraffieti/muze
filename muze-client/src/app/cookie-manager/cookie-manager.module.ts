import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CookieService } from 'ngx-cookie';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class CookieManagerModule {

  private coockieName = "muze-cookie";
  private currentCookie: JSON;

  constructor(private cookieService: CookieService) {}

  /*
   * Returns the rate of the given portrait. If no rate returns 0.
  */
  public getRate(portraitID: string): number {
    const _cookie = this.cookieService.get(this.coockieName);
    if (_cookie != undefined) {
      this.currentCookie = JSON.parse(_cookie);
      const rates = this.currentCookie["rates"];
      for (let item in rates) {
        if(rates[item].portrait == portraitID) {
          return rates[item].rate;
        }
      }
      return 0;
    } else {
      this.currentCookie = JSON.parse(`{"rates":[], "lang": "en"}`);
      return 0;
    }
  }

  public rate(portraitID: string, rate: number) {
    this.currentCookie["rates"].push({"portrait": portraitID, "rate": rate});
    this.cookieService.put("muze-cookie", JSON.stringify(this.currentCookie));
  }

  public getLanguage(): string {
    const _cookie = this.cookieService.get(this.coockieName);
    if (_cookie != undefined) {
      this.currentCookie = JSON.parse(_cookie);
      return this.currentCookie["lang"];

    } else {
      this.currentCookie = JSON.parse(`{"rates":[], "lang": "en"}`);
      return "en";
    }
  }

  public setLanguage(lang: string) {
    this.currentCookie["lang"] = lang;
    this.cookieService.put("muze-cookie", JSON.stringify(this.currentCookie));
  }

}
