import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Ng2DeviceService } from 'ng2-device-detector';

@Injectable()
export class CanActivateViaDesktop implements CanActivate {

  private isMobile: boolean;

  constructor(private deviceService: Ng2DeviceService) {
    this.isMobile = deviceService.isMobile();
  }
  canActivate() {
    return !this.isMobile;
  }
}
