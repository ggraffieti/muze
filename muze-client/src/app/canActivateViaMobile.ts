import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Ng2DeviceService } from 'ng2-device-detector';

@Injectable()
export class CanActivateViaMobile implements CanActivate {

  private isMobile: boolean;

  constructor(private deviceService: Ng2DeviceService, private router: Router) {
    this.isMobile = deviceService.isMobile();
  }
  canActivate() {
    if(this.isMobile) {
        return true;
    } else {
      this.router.navigate(['/desktop']);
      return false;
    }
  }
}
