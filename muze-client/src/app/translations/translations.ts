import {InjectionToken} from '@angular/core';
import { LANG_EN_NAME, LANG_EN_TRANS } from './lang-en';
import { LANG_IT_NAME, LANG_IT_TRANS } from './lang-it';
import { LANG_FR_NAME, LANG_FR_TRANS } from './lang-fr';

export const TRANSLATIONS = new InjectionToken('translations');

const dictionary = {};
dictionary[LANG_EN_NAME] = LANG_EN_TRANS;
dictionary[LANG_IT_NAME] = LANG_IT_TRANS;
dictionary[LANG_FR_NAME] = LANG_FR_TRANS;

export const TRANSLATION_PROVIDERS = [
  { provide: TRANSLATIONS, useValue: dictionary },
];
