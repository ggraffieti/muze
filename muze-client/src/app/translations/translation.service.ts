import {Injectable, Inject} from '@angular/core';
import { TRANSLATIONS } from './translations';

@Injectable()
export class TranslateService {

  private PLACEHOLDER = '%';
  private _defaultLang = 'en';
  private _currentLang = 'en';
  private _fallback = true;

  constructor(@Inject(TRANSLATIONS) private _translations: any) {}

  public get currentLang() {
    return this._currentLang || this._defaultLang;
  }

  public setDefaultLang(lang: string) {
    this._defaultLang = lang;
  }

  public enableFallback(enable: boolean) {
    this._fallback = enable;
  }

  private translate(key: string): string {
    const translation = key;
    // found in current language
    if (this._translations[this.currentLang] && this._translations[this.currentLang][key]) {
      return this._translations[this.currentLang][key];
    }
    // fallback disabled
    if (!this._fallback) {
      return translation;
    }
    // found in default language
    if (this._translations[this._defaultLang] && this._translations[this._defaultLang][key]) {
      return this._translations[this._defaultLang][key];
    }
    // not found
    return translation;
  }

  public use(lang: string): void {
    this._currentLang = lang;
  }

  public instant(key: string, words?: string | string[]) { // add optional parameter
    const translation: string = this.translate(key);
    if (!words) {
      return translation;
    }
    return this.replace(translation, words); // call replace function
  }

  public replace(word: string = '', words: string | string[] = '') {
    let translation: string = word;
    const values: string[] = [].concat(words);
    values.forEach((e, i) => {
      translation = translation.replace(this.PLACEHOLDER.concat(<any>i), e);
    });
    return translation;
  }

}
