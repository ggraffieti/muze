export const LANG_FR_NAME = 'fr';

export const LANG_FR_TRANS = {
  'hello world': 'bonjour le monde',
  'no-rate title': 'Donnez votre avis!',
  'rate title': 'Avez-vous attribué %0 étoiles!',
  'waitMsg': 'La voix est chargée',
  'select language': 'Choisir la langue',

  'welcome': 'Bienvenue à',
  'buy ticket': 'Acheter le ticket sur votre smartphone, économiser votre argent et resquiller!',
  'buy': 'Acheter',
  'buyT': 'Achetez votre billet',
  'name': 'Prénom',
  'surname': 'Nom de Famille',
  'cc': 'Carte de Crédit',
  'ccN': 'Numéro de Carte de Crédit',
  'submit': 'Payer',
  'nameReq': 'Le nom est requis',
  'surnameReq': 'Le nom de famille est requis',
  'ccReq': 'Le Carte de Crédit est requis',
  'ccNReq': 'Le numéro de carte de crédit est requis',
  'qrMessage': 'Montrez ce QR code à la billetterie, et profitez de votre visite.'
};
