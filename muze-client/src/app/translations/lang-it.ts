export const LANG_IT_NAME = 'it';

export const LANG_IT_TRANS = {
  'hello world': 'ciao mondo',
  'no-rate title': 'Facci sapere la tua opinione!',
  'rate title': 'Hai assegnato %0 stelle!',
  'waitMsg': 'Attendere, caricamento sintesi vocale',
  'select language': 'Seleziona la lingua',

  'welcome': 'Benvenuto al',
  'buy ticket': 'Aquista il biglietto sul tuo smartphone, risparmia e salta la fila!',
  'buy': 'Acquista',
  'buyT': 'Acquista il tuo biglietto',
  'name': 'Nome',
  'surname': 'Cognome',
  'cc': 'Carta di Credito',
  'ccN': 'Numero Carta di Credito',
  'submit': 'Paga',
  'nameReq': 'Il nome è obbligatorio',
  'surnameReq': 'Il cognome è obbligatorio',
  'ccReq': 'La carta di credito è obbligatoria',
  'ccNReq': 'Il numero della carta di credito è obbligatorio',
  'qrMessage': 'Mostra questo QR code alla cassa, e goditi la tua visita.'
};
