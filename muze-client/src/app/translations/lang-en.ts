export const LANG_EN_NAME = 'en';

export const LANG_EN_TRANS = {
  'hello world': 'hello world',
  'no-rate title': 'Let us know your opinion!',
  'rate title': 'You rated %0 stars!',
  'waitMsg': 'Voice is being loaded',
  'select language': 'Select language',

  'welcome': 'Welcome to',
  'buy ticket': 'Buy the ticket on your smartphone, save your money and cut the line!',
  'buy': 'Buy',
  'buyT': 'Buy your ticket',
  'name': 'Name',
  'surname': 'Surname',
  'cc': 'Credit Card',
  'ccN': 'Credit Card Number',
  'submit': 'Pay',
  'nameReq': 'The name is required',
  'surnameReq': 'The surname is required',
  'ccReq': 'The credit card is required',
  'ccNReq': 'The credit card number is required',
  'qrMessage': 'Show this QR code at the ticket office, and enjoy your tour.'
};
