import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PortraitComponent } from '../portrait/portrait.component';
import { Page404Component } from '../page404/page404.component';
import { ShowPortraitComponent } from "../show-portrait/show-portrait.component";
import { DesktopComponent } from '../desktop/desktop.component';
import { BuyTicketComponent } from '../buy-ticket/buy-ticket.component';
import { PaymentComponent } from '../payment/payment.component';
import { QRticketComponent } from '../qrticket/qrticket.component';
import { CanActivateViaMobile } from '../canActivateViaMobile';
import { CanActivateViaDesktop } from '../canActivateViaDesktop';
import { CheckPayment } from '../checkPayment';


const routes: Routes = [
  { path: 'portrait/:id', component: PortraitComponent, canActivate: [CanActivateViaMobile]},
  { path: 'image/:id', component: ShowPortraitComponent, canActivate: [CanActivateViaMobile]},
  { path: 'buyticket', component: BuyTicketComponent, canActivate: [CanActivateViaMobile]},
  { path: 'payment', component: PaymentComponent, canActivate: [CanActivateViaMobile]},
  { path: 'qrticket', component: QRticketComponent, canActivate: [CanActivateViaMobile, CheckPayment]},
  { path: 'desktop', component: DesktopComponent, canActivate: [CanActivateViaDesktop]},
  { path: '**', component: Page404Component, canActivate: [CanActivateViaMobile]}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
