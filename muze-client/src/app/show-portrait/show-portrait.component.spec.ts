import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowPortraitComponent } from './show-portrait.component';

describe('ShowPortraitComponent', () => {
  let component: ShowPortraitComponent;
  let fixture: ComponentFixture<ShowPortraitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowPortraitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPortraitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
