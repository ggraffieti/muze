import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServerConfig } from "../server-config";

@Component({
  selector: 'app-show-portrait',
  templateUrl: './show-portrait.component.html',
  styleUrls: ['./show-portrait.component.scss']
})
export class ShowPortraitComponent implements OnInit {

  private id: string;
  private imgURL: string;
  private serverConfig: ServerConfig = new ServerConfig();;

  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe(params => this.id = params['id']);
    this.imgURL = this.serverConfig.getServerAddress() + "/img/" + this.id;
  }

  ngOnInit() {
  }

}
