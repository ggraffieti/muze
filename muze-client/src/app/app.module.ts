import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Ng2DeviceDetectorModule } from 'ng2-device-detector';
import { CookieModule } from 'ngx-cookie';
import { ChartsModule } from 'ng2-charts';
import { TranslateService, TranslatePipe, TRANSLATION_PROVIDERS } from './translations';

import { AppComponent } from './app.component';
import { PortraitComponent } from './portrait/portrait.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { DesktopComponent } from './desktop/desktop.component';
import { Page404Component } from './page404/page404.component';
import { RatesComponent } from './rates/rates.component';
import { TranslateComponent } from './translate/translate.component';
import { ShowPortraitComponent } from './show-portrait/show-portrait.component';
import { CookieManagerModule } from './cookie-manager/cookie-manager.module';
import { CanActivateViaMobile } from './canActivateViaMobile';
import { CanActivateViaDesktop } from './canActivateViaDesktop';
import { CheckPayment } from './checkPayment';
import { BuyTicketComponent } from './buy-ticket/buy-ticket.component';
import { TextToSpeechModule } from './text-to-speech/text-to-speech.module';
import { PaymentComponent } from './payment/payment.component';
import { QRticketComponent } from './qrticket/qrticket.component';
import { TranslationManagerModule } from './translation-manager/translation-manager.module';

@NgModule({
  declarations: [
    AppComponent,
    PortraitComponent,
    DesktopComponent,
    Page404Component,
    RatesComponent,
    TranslatePipe,
    TranslateComponent,
    ShowPortraitComponent,
    BuyTicketComponent,
    PaymentComponent,
    QRticketComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    Ng2DeviceDetectorModule.forRoot(),
    ChartsModule,
    TextToSpeechModule,
    TranslationManagerModule,
    CookieModule.forRoot(),
    CookieManagerModule
  ],
  providers: [
    TRANSLATION_PROVIDERS,
    TranslateService,
    CanActivateViaMobile,
    CanActivateViaDesktop,
    CheckPayment
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
