import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TranslationManagerModule } from '../translation-manager/translation-manager.module';
import { TextToSpeechModule } from '../text-to-speech/text-to-speech.module';

@Component({
  selector: 'app-translate',
  templateUrl: './translate.component.html',
  styleUrls: ['./translate.component.scss']
})
export class TranslateComponent implements OnInit {

  @Output() switchLang = new EventEmitter<void>();
  public supportedLanguages: any[];

  constructor(private _translate: TranslationManagerModule, private textToSpeech: TextToSpeechModule) { }

  ngOnInit() {
    this.supportedLanguages = [
      { display: 'English', value: 'en' },
      { display: 'Italiano', value: 'it' },
      { display: 'Français', value: 'fr' },
    ];
  }

  selectLang(lang: string) {
    this._translate.setLanguage(lang);
    this.switchLang.emit();
    this.textToSpeech.changeVoice(lang);
  }

}
