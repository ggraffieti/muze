import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page404',
  templateUrl: './page404.component.html',
  styleUrls: ['./page404.component.scss']
})
export class Page404Component implements OnInit {

  private message: string;

  constructor() {
    switch (navigator.language) {
      case "it": this.message = "Pagina non trovata, continua a cercare..."; break;
      case "en": this.message = "Page not found, keep searching..."; break;
      case "fr": this.message = "Page non trouvée, continue à essayer..."; break;
      default: this.message = "Page not found, keep searching...";
    }
  }

  ngOnInit() {
  }

}
