export class ServerConfig {
  private serverIP = "http://localhost";
  private serverPort = 6041;

  constructor() {}

  public getServerIP(): string {
    return this.serverIP;
  }

  public getServerPort(): number {
    return this.serverPort;
  }

  public getServerAddress():string {
    return this.serverIP + ":" + this.serverPort;
  }
}
