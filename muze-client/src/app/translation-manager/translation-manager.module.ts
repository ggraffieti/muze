import {EventEmitter, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateService } from '../translations/translation.service';
import { CookieManagerModule } from '../cookie-manager/cookie-manager.module';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})

export class TranslationManagerModule {

  public onLangChanged: EventEmitter<string> = new EventEmitter<string>();

  constructor(private _translate: TranslateService, private cookieManger: CookieManagerModule) {
    this._translate.use(this.cookieManger.getLanguage());
  }

  public get language(): string {
    return this._translate.currentLang;
  }

  public setLanguage(lang: string) {
    if (!(this._translate.currentLang === lang)) {
      this._translate.use(lang);
      this.onLangChanged.emit(lang);
      this.cookieManger.setLanguage(lang);
    }
  }

  public enableFallback(enable: boolean) {
    this._translate.enableFallback(enable);
  }

}
