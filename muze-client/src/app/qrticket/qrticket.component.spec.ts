import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QRticketComponent } from './qrticket.component';

describe('QRticketComponent', () => {
  let component: QRticketComponent;
  let fixture: ComponentFixture<QRticketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QRticketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QRticketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
