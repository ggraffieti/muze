import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-qrticket',
  templateUrl: './qrticket.component.html',
  styleUrls: ['./qrticket.component.scss']
})
export class QRticketComponent implements OnInit {

  private img = "spinnerQR.svg";
  private loaded = false;

  constructor() {
    this.showQR();
  }

  ngOnInit() {
  }

  private showQR() {
    setTimeout(() => {
      this.img = "qrcode.png";
      this.loaded = true;
    }, 2500)
  }

}
