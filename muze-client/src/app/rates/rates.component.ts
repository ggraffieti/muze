import { Component, OnInit, Input } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { CookieManagerModule } from '../cookie-manager/cookie-manager.module'
import { ServerConfig } from "../server-config";

@Component({
  selector: 'app-rates',
  templateUrl: './rates.component.html',
  styleUrls: ['./rates.component.scss']
})
export class RatesComponent implements OnInit {

  @Input() portraitID: string;

  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    legend: {
      display: false
    },
    scales: {
      xAxes: [{
        ticks: {
          beginAtZero: true,
          fontColor: 'black',
          fontSize: '15',
          fontWeight: 'bold',
          fontFamily: 'sans'
        }
      }],
      yAxes: [{
        barPercentage: 0.7,
        ticks: {
          fontColor: 'black',
          fontFamily: 'sans',
          fontSize: '15',
          fontWeight: 'bold'
        }
      }]
    },
    responsive: true
  };
  public barChartLabels:string[] = ['5 ★', '4 ★', '3 ★', '2 ★', '1 ★'];
  public barChartType:string = 'horizontalBar';

  public barChartData:any[] = [
    {data: []}
  ];

  public chartColors: any[] = [
    {
      backgroundColor: ["#590200", "#960400", "#C30C07", "#ED3530", "#FF7572"]
    }
  ];

  private rated: boolean = false;
  private portraitRate: number = 0;
  private serverConfig: ServerConfig = new ServerConfig();

  constructor(private http: Http, private cookieManger: CookieManagerModule) {

  }

  ngOnInit() {
    this.http.get(this.serverConfig.getServerAddress() + "/ratings/" + this.portraitID)
      .map((res) => this.updateGraph(res))
      .subscribe();


    this.portraitRate = this.cookieManger.getRate(this.portraitID);
    if (this.portraitRate == 0) {
      this.rated = false;
    }
  }

  private rate(star: number) {
    if (!this.rated) {
      this.portraitRate = star;
      const body = 'portrait=' + this.portraitID + '&rate=' + this.portraitRate;
      const head = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
      this.http.post(this.serverConfig.getServerAddress() + '/ratings', body, {headers: head})
        .map((res) => this.updateGraph(res))
        .subscribe();
      this.cookieManger.rate(this.portraitID, star);
      this.rated = true;
    } // else notifica gia votato
    else {
      console.log("gia votato");
    }
  }

  private updateGraph(res: Response) {
    let body = res.json();
    this.barChartData = [
      {data: [body.fiveStars, body.fourStars, body.threeStars, body.twoStars, body.oneStar]}
    ];
  }
}
