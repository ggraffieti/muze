# Muze
A new concept of visiting museums.

## License 
Muze is an open source application made available under the GPLv3 license.
See LICENSE for details.

Copyright (C) 2017  Alfredo Maffi

Copyright (C) 2017  Gabriele Graffieti

Copyright (C) 2017  Manuel Peruzzi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Import
* First fork this repository, then go to your fork repo and `git clone <your-fork-url>`
* `cd <your-project-folder>` You should see 3 directories: database, muze-client and muze-server
* Now restore the database from dumped data, located in `database/dump`
    * First assure that you have installed `mongo`, `mongod` and `mongorestore` commands.
    * Open a shell, in the database directory, and type `mongod -dbpath ./workingDir` If workingDir is not present, create it.
    * Open another shell in the same directory, and type `mongorestore --drop -d muzedb ./dump` for restoring the database. **The database name have to be muzedb**.
    * Type `mongo` to open mongodb console. If the database is correctly imported, you should see it with `show dbs`. With `use muzedb` and `show collections` You shold see three collections _for now_ (artists, portraits and ratings).
* The database is restored, now open a shell in `muze-server` directory and type `npm install` to install dependencies. If something doesn't work, or you are unable to download dependencies, please, open an issue or contact one member of the team.
* After that open a shell in the `muze-server` directory and type `npm start`. The server should start at the port 6041. If not open an issue or contact one member of the team (probably you have to compile typescript in javascript. Use `tcs` command or your favorite editor).
* Now the server is working! open a shell in the directory `muze-client` and type `npm install` to download dependencies, and `ng serve --port 1406 --host <your-ip>` to build and pull up the client server. It should listen at port 1406.
* Ok, if nothing went wong, you have successfully imported, built and opened the muze app! Try it by opening your browser, and typing in the address bar `<your-ip>:1406/portrait/monalisa`. If details are not showed, go to `muze-client/src/app/server-config.ts` and change `serverIP` variable to `<your-ip>`. Please, do not push this change.
* Have fun :smiley:

### Working with git
* Add the remote "truth" repo, because you have to pull from it `git remote add hebry https://bitbucket.org/ggraffieti/muze`
* Working on git normally, push changes on your fork repo `git push (origin master)` (origin master can be avoided), and, when you finish your work do a pull request to the truth repo. Every day, before start working do a pull from the truth repo `git pull hebry master` for receive recent changes.

### Working with MongoDB
First and foremost, be sure to run a mongo server instance using the `mongod` command within a shell (you can optionally specify the working directory with `mongod -dbpath <pathToDir>`). Subsequently, run the `mongo` command in a separate shell in order to work with the mongo shell and be able to perform all the following operations.

#### Database Control & Management
* `show dbs` : shows all the databases contained in the mongo server's working directory.
* `use <db_name>` : creates a new database named <db_name> and switches control to it, which means that all the following operations will be performed on such database. If a database with <db_name> name already exists, only the latter operation is performed.

#### Collections & Documents Operations
* `show collections` : shows all the current db's collections.
* `db.createCollection(<c_name>, {<preferences>})` : creates a brand new collection named <c_name> in the current database. The optional json-like <preferences> parameter is used to costumize the collection, see documentation @ https://goo.gl/tmksKK
* `db.<c_name>.insertOne(<document>)` : inserts the specified <document> into the <c_name> collection.
* `db.<c_name>.insertMany(<d1>,<d2>,...)` : inserts all the specified documents into the <c_name> collection.
* `db.<c_name>.updateOne(<filter>, <document>, <preferences>)` : updates the first document, which matches with the <filter> parameter, with the specified <document>.
* `db.<c_name>.updateMany(<filter>, <document>, <preferences>)` : same as above, but applied to all the matching documents.
* `db.<c_name>.deleteOne(<filter>, <preferences>)` : deletes the first document which matches with the <filter> parameter.
* `db.<c_name>.deleteMany(<filter>, <preferences>)` : same as above, but applied to all matching documents.
* `db.<c_name>.find()` : returns all the collection <c_name>'s documents.
* `db.<c_name>.find(<filter>)` : returns all the documents in the <c_name> collection which match with the <filter> parameter.

#### Database Dump & Restore
The following commands must be executed from a normal shell (not the mongo one).

* `mongodump -d <db_name> -o <path_to_dir>` : simpliest way to perform a database dump in a specific directory. To perform a custom dump, see documentation @ https://goo.gl/m4l0C0
* `mongorestore -d <db_name> <path_to_dump>` : restores a database from a previosly created dump. For more information/customization, see documentation @ https://goo.gl/1EmJgv

### Commands lo launch the services
* **DataBase** -> `mongod -dbpath ...muze/database/workingDir`
* **Server** -> `npm start`
* **Client Angular** -> `ng serve --port 1406 --host <your-ip>`
    * Note, you should change the ip of the server in the file /muze/muze-client/src/app/server-config.ts

### Ports
* **SERVER** -> 6041
* **CLIENT** -> 1406
* **DBSERVER** -> 27017