import {ratings} from "./ratingsModel";
import * as assert from "assert";

export class RatingsHandler {

    public static getRatings(id: string, next: (s: string) => void) {
        ratings.findOne().where('portrait', id).exec((err, doc) => {
            if(err || !doc) {
                next(JSON.stringify({}))
            } else {
                next(JSON.stringify({
                    oneStar: doc.oneStar,
                    twoStars: doc.twoStars,
                    threeStars: doc.threeStars,
                    fourStars: doc.fourStars,
                    fiveStars: doc.fiveStars
                }))
            }
        })
    }

    public static setRate(id: string, rate: number, next: (s: string) => void) {
        ratings.findOne().where('portrait', id).exec((err, doc) => {
            assert.equal(err, null);
            if(!doc) {
                this.createPortraitRatings(id, rate, next)
            } else {
                switch(rate) {
                    case 1:
                        doc.oneStar++;
                        break;
                    case 2:
                        doc.twoStars++;
                        break;
                    case 3:
                        doc.threeStars++;
                        break;
                    case 4:
                        doc.fourStars++;
                        break;
                    case 5:
                        doc.fiveStars++;
                        break;
                }
                doc.save();
                next(JSON.stringify({
                    oneStar: doc.oneStar,
                    twoStars: doc.twoStars,
                    threeStars: doc.threeStars,
                    fourStars: doc.fourStars,
                    fiveStars: doc.fiveStars
                }))
            }
        })
    }

    private static createPortraitRatings(id: string, rate: Number, next: (s: string) => void) {
        ratings.create({
            portrait: id,
            oneStar: rate == 1,
            twoStars: rate == 2,
            threeStars: rate == 3,
            fourStars: rate == 4,
            fiveStars: rate == 5
        }, (err, doc) => {
            assert.equal(err, null);
            next(JSON.stringify({
                oneStar: doc.oneStar,
                twoStars: doc.twoStars,
                threeStars: doc.threeStars,
                fourStars: doc.fourStars,
                fiveStars: doc.fiveStars
            }))
        })
    }

}

