const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const RatingsSchema = new mongoose.Schema({
    portrait: String,
    oneStar: Number,
    twoStars: Number,
    threeStars: Number,
    fourStars: Number,
    fiveStars: Number
});

export const ratings = mongoose.model("ratings", RatingsSchema);