const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const portraitsSchema = new mongoose.Schema({
    _id: String,
    name: String,
    artist: String,
    year: Number,
    img: String,
    description_it: String,
    description_en: String,
    description_fr: String
});

export const portraits = mongoose.model('portraits', portraitsSchema);
