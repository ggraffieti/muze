import {portraits} from "./portraitsModel";
import {artists} from "../Artists/artistsModel";

export class PortraitsHandler {

    public static getInfo(id: string, lang: string, next: (s:string) => void) {
        portraits.findOne().where('_id', id).exec((pErr, pDoc) => {
            if(pErr || !pDoc) {
                next(JSON.stringify({}))
            } else {
                artists.findOne().where('_id', pDoc.artist).exec((aErr, aDoc) => {
                    let artist = "Anonymous";
                    if(!aErr || aDoc) {
                        artist = aDoc.name;
                    }
                    let desc: string;
                    switch(lang) {
                        case 'en':
                            desc = pDoc.description_en;
                            break;
                        case 'it':
                            desc = pDoc.description_it;
                            break;
                        case 'fr':
                            desc = pDoc.description_fr;
                            break;
                        default:
                            desc = pDoc.description_en;
                            break;
                    }
                    next(JSON.stringify({
                        name: pDoc.name,
                        artist: artist,
                        year: pDoc.year,
                        description: desc
                    }))
                })
            }
        })
    }

    public static getImage(id: string, next: (s: string) => void) {
        portraits.findOne().where('_id', id).exec((err, doc) => {
            if(err || !doc) {
                next("")
            } else {
                next(doc.img);
            }
        })
    }

}
