import * as express from "express";
import * as path from "path";
import {DatabaseAccess} from "./databaseAccess";

let app = express();
let bodyParser = require("body-parser");
let db = new DatabaseAccess();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/info/:id', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin','*');
    db.getInfo(req.params.id, req.query.lang, (r) => res.send(r));
});

app.get('/img/:id', (req, res) => {
    res.setHeader('Content-Type', 'image/jpg');
    res.setHeader('Access-Control-Allow-Origin','*');
    db.getImage(req.params.id, (r) => res.sendFile(path.resolve(r)));
});

app.get('/artist/:id', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin','*');
    db.getArtist(req.params.id, (r) => res.send(r));
});

app.get('/ratings/:id', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin','*');
    db.getRatings(req.params.id, (r) => res.send(r));
});

app.post('/ratings', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin','*');
    db.setRate(req.body.portrait, parseInt(req.body.rate), (r) => res.send(r));
});

app.listen(6041, function () {
    console.log('Server listening on port 6041!');
});
