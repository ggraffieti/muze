import {PortraitsHandler} from "./Portraits/portraitsHandler"
import {ArtistsHandler} from "./Artists/artistsHandler";
import {RatingsHandler} from "./Ratings/ratingsHandler";

const mongoose = require('mongoose');

export class DatabaseAccess {

    private static url = 'mongodb://localhost:27017/muzedb';

    constructor() {
        mongoose.connect(DatabaseAccess.url);
        mongoose.connection.on('error', console.error.bind(console, 'connection error:'))
    }

    public getInfo(id: string, lang: string, next: (s: string) => void) {
        PortraitsHandler.getInfo(id, lang, next);
    }

    public getImage(id: string, next: (s: string) => void) {
        PortraitsHandler.getImage(id, next);
    }

    public getArtist(id: string, next: (s: string) => void) {
        ArtistsHandler.getArtist(id, next);
    }

    public getRatings(id: string, next: (s: string) => void) {
        RatingsHandler.getRatings(id, next);
    }

    public setRate(id: string, rate: number, next: (s: string) => void) {
        RatingsHandler.setRate(id, rate, next);
    }

}


