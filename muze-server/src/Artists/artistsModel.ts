const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const artistsSchema = new mongoose.Schema({
    _id: String,
    name: String,
    birth: Date,
    city: String
});

export const artists = mongoose.model('artists', artistsSchema);
