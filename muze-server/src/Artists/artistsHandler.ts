import {artists} from "./artistsModel";

export class ArtistsHandler {

    public static getArtist(id: string, next: (s: string) => void) {
        artists.findOne().where('_id', id).exec((err, doc) => {
            if(err || !doc) {
                next(JSON.stringify({}))
            } else {
                next(JSON.stringify({
                    name: doc.name,
                    birth: doc.birth,
                    city: doc.city
                }))
            }
        })
    }

}
